package com.epam.engx.story.delivery;

import java.util.function.Supplier;

/**
 * Implements lazy initialization for any object type.
 *
 * <p>This class allows you to delay the creation of an object until it's first needed. It provides
 * a generic way to achieve lazy initialization.
 *
 * @param <T> The type of object to be lazily initialized.
 */
public class Lazy<T> {
  private T obj;
  private final Supplier<T> creator;

  public Lazy(Supplier<T> creator) {
    this.creator = creator;
  }

  public synchronized T get() {
    if (obj == null) {
      obj = creator.get(); // Creates instance only when it’s required
    }
    return obj;
  }
}
