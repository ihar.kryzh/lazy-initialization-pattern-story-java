package com.epam.engx.story.delivery;

public class AddressValidator {
  private final Lazy<USAddressValidator> validator; // Holds a lazy instance

  public AddressValidator() {
    validator = new Lazy<>(USAddressValidator::new); // Expensive creation logic
  }

  public boolean validateAddress(String address) {
    return validator.get().validate(address); // Access the USAddressValidator instance
  }
}
